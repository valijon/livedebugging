FROM maven:3.6.1-jdk-13-alpine as build
WORKDIR /workspace/livedebugging
LABEL intermidiate_frs=true
COPY pom.xml .
RUN mvn -B clean install -DskipTests -Dcheckstyle.skip -Dasciidoctor.skip -Djacoco.skip -Dmaven.gitcommitid.skip -Dspring-boot.repackage.skip -Dmaven.exec.skip=true -Dmaven.install.skip -Dmaven.resources.skip
COPY . .
RUN mvn package -Dmaven.test.skip=true -Dmaven.site.skip=true -Dmaven.javadoc.skip=true

FROM bellsoft/liberica-openjdk-alpine:11 as livedebugging
ARG DIR=/workspace/livedebugging
COPY --from=build ${DIR}/target/*.jar /home/app.jar
ENTRYPOINT ["java","-jar","-agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=*:5005", "/home/app.jar"]