package com.example.livedebugging;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LivedebuggingApplication {

    public static void main(String[] args) {
        SpringApplication.run(LivedebuggingApplication.class, args);
    }
}
