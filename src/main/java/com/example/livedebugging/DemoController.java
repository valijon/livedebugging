package com.example.livedebugging;

import java.util.Date;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/demo")
public class DemoController {

    @GetMapping("/say/hello")
    public String sayHello(@RequestParam String name, @RequestParam int id) {
        if (name.equals("valijon")) {
            throw new RuntimeException("Something went wrong...");
        }
        return String.format("Hello %s!", name);
    }

    @GetMapping("/status")
    public String systemStatus() {
        return "I'm working...\n" + new Date();
    }

    @GetMapping("/ping")
    public String pingPong() {
        return "pong\n" + new Date();
    }
}
